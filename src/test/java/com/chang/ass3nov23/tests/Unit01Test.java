package com.chang.ass3nov23.tests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.chang.assignment2Bean.*;
//import com.chang.assignment2nov11copy.*;
//import com.chang.assigment2nov11.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cha_xi
 */
public class Unit01Test {

    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "dawson";    
//    private final String url = "jdbc:mysql://localhost:3306";
//    private final String user = "root";
//    private final String password = "concordia";
    ///////////////////
    private MedBean medB;
    private Patient patient;
    private InPatientBean inPatientB;
    private SurgicalBean surgicalB;
    private Patient patient2;
    private InPatientBean inPatientB2;
    private SurgicalBean surgicalB2;
    private MedBean medB2;
    private final PatientDAO pDAOImpl = new PatientDAOImpl();
    private final PatientDAO pDAOImpl2 = new PatientDAOImpl();
    private final PatientDAO pDAOImpl3 = new PatientDAOImpl();

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    @Rule
    public MethodLogger methodLogger = new MethodLogger();

// INSERT INTO MEDICATION (ID, PATIENTID, DATEOFMED, MED, UNITCOST, UNITS) VALUES (-112,2,2002-5-5 8:8:8,ewes,0.1,1.2)
    public void init() {

        Timestamp dateOfMed = Timestamp.valueOf("2000-2-2 1:1:1");
        Timestamp admissionDate = Timestamp.valueOf("1999-3-3 23:59:59");
        Timestamp releaseDate = Timestamp.valueOf("2001-4-4 2:2:2");
        Timestamp dateOfStay = Timestamp.valueOf("2002-5-5 8:8:8");
        Timestamp dateOfSurgery = Timestamp.valueOf("2009-6-6 9:9:9");

        medB = new MedBean(-1, 2, dateOfMed, "medtest", 0.1, 0.2);
        System.out.println("medB="+medB.toString());
                
        patient = new Patient(-11, "LastNameTest", "FirstNameTest", "TestDiagnosis", admissionDate, releaseDate);
        inPatientB = new InPatientBean(333, 2, dateOfStay, "ABB", 0.01, 1.01, 2.33);
        surgicalB = new SurgicalBean(1, 2, dateOfSurgery, "surgery", 100.1, 10.1, 1.99);

        patient2 = new Patient(-11, "LastNameUpdate", "FirstNameUpdate", "TestDiagnosisUpdate", admissionDate, releaseDate);
        medB2 = new MedBean(-1, 2, dateOfMed, "medUpdate", 0.1, 0.2);
        inPatientB2 = new InPatientBean(333, 2, dateOfStay, "Ue", 0.01, 1.01, 2.33);
        surgicalB2 = new SurgicalBean(1, 2, dateOfSurgery, "Update", 100.1, 10.1, 1.99);
    }

    /**
     *
     * insert a patient and find it by another pDAOImpl delete a patient and
     * find it by another pDAOImpl update a patient and find it by another
     * pDAOImpl
     *
     * @throws SQLException
     */
    /**
     * test Patient for create, find, update and delete
     * @throws SQLException 
     */
    @Test
    public void testPatientFindAll() throws SQLException{
        log.info("testPatientFindAll");
        ObservableList<Patient> allPatient = FXCollections.observableArrayList();
        allPatient = pDAOImpl.findAll();
        int i;
        for(i=0; i<allPatient.size();i++){
            System.out.println(allPatient.get(i).toString());
        }
        assertTrue(allPatient.size()>0);
    }
    
    @Test
    public void testInPatientFindAll() throws SQLException{
        log.info("testInPatientFindAll");
        ArrayList<InPatientBean> allInPatient = new ArrayList<InPatientBean>();
        allInPatient = pDAOImpl.findAllInPatient();
        int i;
        for(i=0; i<allInPatient.size();i++){
            System.out.println(allInPatient.get(i).toString());
        }
        assertTrue(allInPatient.size()>0);
    }    
    
    @Test
    public void testMedFindAll() throws SQLException{
        log.info("testMedFindAll");
        ArrayList<MedBean> allMed = new ArrayList<MedBean>();
        allMed = pDAOImpl.findAllMed();
        int i;
        for(i=0; i<allMed.size();i++){
            System.out.println(allMed.get(i).toString());
        }
        assertTrue(allMed.size()>0);
    }
    
    @Test
    public void testSurgicalFindAll() throws SQLException{
        log.info("testSurgicalFindAll");
        ArrayList<SurgicalBean> allSurgical = new ArrayList<SurgicalBean>();
        allSurgical = pDAOImpl.findAllSurgical();
        int i;
        for(i=0; i<allSurgical.size();i++){
            System.out.println(allSurgical.get(i).toString());
        }
        assertTrue(allSurgical.size()>0);
    }    
    
    @Test
    public void testPatientCreate() throws SQLException {
        pDAOImpl2.insertPatient(patient);
        assertEquals(patient, pDAOImpl.findById(patient.getPatientID()));
    }

//delete patient and find by another pDAOImpl
    @Test
    public void testPatientUpdate() throws SQLException {
        pDAOImpl.insertPatient(patient);
        pDAOImpl3.updatePatient(patient2);
        assertTrue(patient2.equals(pDAOImpl2.findById(patient2.getPatientID())));
    }

    public void testPatientDelete() throws SQLException {
        pDAOImpl.insertPatient(patient);
        pDAOImpl3.deletePatient(patient);
        assertNull(pDAOImpl3.findById(patient.getPatientID()));
    }
/*
    create inPatient and obtain new id
    */
    @Test
    public void testInPatientCreate() throws SQLException {
        pDAOImpl2.insertInp(inPatientB);
        int oldId = inPatientB.getId();
        int newId = pDAOImpl.fixInPatientBeanID(inPatientB);
        System.out.println("old ID for inPatient is "+oldId+" new Id is "+newId);
        System.out.println("created inPatient is");
        System.out.println(pDAOImpl3.findInPatientBeanById(newId).toString());
        System.out.println("supposed to be");
        System.out.println(inPatientB.toString());
        assertTrue(inPatientB.equals(pDAOImpl3.findInPatientBeanById(newId)));
    }
    /*
    renew id and update inpatient
    */
    
    @Test
    public void testInPatientUpdate() throws SQLException {
        pDAOImpl.insertInp(inPatientB);
        int newId = pDAOImpl2.fixInPatientBeanID(inPatientB);
        inPatientB2.setId(newId);
        pDAOImpl3.updateInp(inPatientB2);
        System.out.println("old inPatient is");
        System.out.println(inPatientB.toString());
        System.out.println("updated inPatient is");
        System.out.println(inPatientB2.toString());
        assertTrue(inPatientB2.equals(pDAOImpl2.findInPatientBeanById(newId)));
    }    
    
    /**
     * insert and then delete
     * @throws SQLException 
     */
    
    @Test
     public void testInPatientDelete() throws SQLException {
        pDAOImpl.insertInp(inPatientB);
        int newId = pDAOImpl2.fixInPatientBeanID(inPatientB);
        pDAOImpl3.deleteInp(newId);
        assertNull(pDAOImpl3.findInPatientBeanById(newId));
    }
     
     /**
      * create medication and obtain id
      * @throws SQLException 
      */
    @Test
    public void testMedCreate() throws SQLException {
        pDAOImpl2.insertMed(medB);
        int oldId = medB.getID();
        int newId = pDAOImpl.fixMedBeanID(medB);
        System.out.println("old ID for Med is "+oldId+" new Id is "+newId);
        assertTrue(medB.equals(pDAOImpl3.findMedBeanById(newId)));
    }
    
    /*
    renew id and update Medication
    */
    @Test
    public void testMedUpdate() throws SQLException {
        pDAOImpl.insertMed(medB);
        int newId = pDAOImpl2.fixMedBeanID(medB);
        medB2.setID(newId);
        pDAOImpl3.updateMed(medB2);
        assertTrue(medB2.equals(pDAOImpl2.findMedBeanById(newId)));
    }  
/**
 * delete medication
 * @throws SQLException 
 */
    @Test
     public void testMedDelete() throws SQLException {
        pDAOImpl.insertMed(medB);
        int newId = pDAOImpl2.fixMedBeanID(medB);
        pDAOImpl3.deleteMed(newId);
        assertNull(pDAOImpl3.findMedBeanById(newId));
    }    

         /**
      * create surgical and obtain id
      * @throws SQLException 
      */
    @Test
    public void testSurgicalCreate() throws SQLException {
        pDAOImpl2.insertSur(surgicalB);
        int oldId = surgicalB.getId();
        int newId = pDAOImpl.fixSurgicalBeanID(surgicalB);
        System.out.println("old ID for surgical is "+oldId+" new Id is "+newId);
        assertTrue(surgicalB.equals(pDAOImpl3.findSurgicalBeanById(newId)));
    }

    /*
    renew id and update surgical
    */
    @Test
    public void testSurgicalUpdate() throws SQLException {
        pDAOImpl.insertSur(surgicalB);
        int newId = pDAOImpl2.fixSurgicalBeanID(surgicalB);
        surgicalB2.setId(newId);
        pDAOImpl3.updateSur(surgicalB2);
        assertTrue(surgicalB2.equals(pDAOImpl2.findSurgicalBeanById(newId)));
    }  
    
    
/**
 * delete medication
 * @throws SQLException 
 */
    @Test
     public void testSurgicalDelete() throws SQLException {
        pDAOImpl.insertSur(surgicalB);
        int oldId = surgicalB.getId();
        int newId = pDAOImpl2.fixSurgicalBeanID(surgicalB);
        System.out.println("oldId in surgical delete is "+oldId+"  new id is "+newId);
        pDAOImpl3.deleteSur(newId);
        assertNull(pDAOImpl3.findSurgicalBeanById(newId));
    }    
     
     @Test
     public void testSurgicalDate() throws SQLException{
         Timestamp timeA = pDAOImpl.findSurgicalBeanById(1).getDateOfSurgery();
         System.out.println(timeA.toString());
         assertEquals(pDAOImpl2.dateSur(1),timeA);
        
     }
     
    /**
     * This routine recreates the database for every test. This makes sure that
     * a destructive test will not interfere with any other test.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss who helped me out last winter with an issue with Arquillian. Look
     * up Arquillian to learn what it is.
     */
    @Before
    public void seedDatabase() {
        init();

///
        final String seedDataScript = loadAsString("hospitaldb.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
