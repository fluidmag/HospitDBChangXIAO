package com.chang.ass3nov23.controllers;

/**
 * Sample Skeleton for 'Scene.fxml' Controller Class
 */
import com.chang.assignment2Bean.*;
import java.sql.SQLException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.scene.layout.AnchorPane;

public class FXMLController {

    /**
     * define a logger for debugging codes
     */
    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    // implement PatientDAOImpl to operating SQL database.
    private PatientDAOImpl pDAOImpl;

    /**
     * data presented in the form
     */
    private int iPresent;
    private String tableName;
    private Patient p;
    private ObservableList<Patient> pArrayList;
    //   private ArrayList<Patient> pArrayListPresent;
    //
    private MedBean m;
    private ArrayList<MedBean> mArrayList;
    //   private ArrayList<MedBean> mArrayListPresent;
    //
    private InPatientBean ip;
    private ArrayList<InPatientBean> ipArrayList;
    //   private ArrayList<InPatientBean> ipArrayListPresent;
    //
    private SurgicalBean s;
    private ArrayList<SurgicalBean> sArrayList;
    //   private ArrayList<SurgicalBean> sArrayListPresent;
    ////

    //@FXML // ResourceBundle that was given to the FXMLLoader
    //private ResourceBundle resources;

    //???????? never use this URL in scene builder, and i cant find it. how can i find it in scene builder?
    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    public FXMLController() {
        super();
        pDAOImpl = new PatientDAOImpl();
        m = new MedBean();
        p = new Patient();
        ip = new InPatientBean();
        s = new SurgicalBean();
        tableName = " ";
        iPresent = 0;

        log.info("END of constructor FXMLController()");

    }
    
    public void setPatientDAOImpl(PatientDAO p){pDAOImpl = (PatientDAOImpl) p;}
 /*
     * As the last row (row for entering info) is binding to one of p,m,ip,s, we
     * just need to clear the content of them. Then it is suppose to
     * automatically clear all info in the input row of the table.
     *
     * @param event
     */
    @FXML
    void clearAll(ActionEvent event) {
        p = new Patient();
        m = new MedBean();
        ip = new InPatientBean();
        s = new SurgicalBean();

        //    pArrayList = new ArrayList();
        //    ipArrayList = new ArrayList();
        //   mArrayList = new ArrayList();
        //    sArrayList = new ArrayList();
        //    iPresent = 0;
    }

    /**
     * delete rows and if not find popup info.
     *
     * @param event
     */
    @FXML
    void deleteRow(ActionEvent event) {
        try {
            String str = " ";
            switch (tableName) {
                case "Patient":
                    str = pDAOImpl.findById(p.getPatientID()).toString();
                    pDAOImpl.deletePatient(p);
                    break;
                case "InPatient":
                    str = pDAOImpl.findInPatientBeanById(ip.getId()).toString();
                    if (pDAOImpl.findInPatientBeanById(ip.getId()).equals(ip)) {
                        pDAOImpl.deleteInp(ip.getId());
                        break;
                    }
                case "Medication":
                    str = pDAOImpl.findMedBeanById(m.getID()).toString();
                    if (pDAOImpl.findMedBeanById(m.getID()).equals(m)) {
                        pDAOImpl.deleteMed(m.getID());
                        break;
                    }
                case "Surgical":
                    str = pDAOImpl.findSurgicalBeanById(s.getId()).toString();
                    if (pDAOImpl.findSurgicalBeanById(s.getId()).equals(s)) {
                        pDAOImpl.deleteSur(s.getId());
                        break;
                    }
                default:
                    log.debug("undefined tableName or not matched info in deleteRow()");
                    //if not find tableName, need to check codes, if not find the rows to delete, need to check input info
                    if (str.equalsIgnoreCase(" ")) {
                        Alert alert1 = new Alert(AlertType.INFORMATION);
                        alert1.setTitle("Warnings!! undefined tableName: " + tableName);
                        alert1.setHeaderText("check codes");
                        alert1.setContentText("check codes in controller for deleteRow()");
                        alert1.showAndWait();
                    } else {
                        Alert alert1 = new Alert(AlertType.INFORMATION);
                        alert1.setTitle("Warnings!! can't find input in the table of" + tableName);
                        alert1.setHeaderText("Check your input info and Suggested input is");
                        alert1.setContentText(str);
                        alert1.showAndWait();
                    }
                    break;
            }
        } catch (SQLException e) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Warnings!! SQL error when using delete in " + tableName + " table");
            alert.setHeaderText("Check your codes in deleteRow");
            alert.setContentText("Check your codes please");
            alert.showAndWait();
        }

        log.debug("End of deleteRow");
    }

    @FXML
    void exitForm(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void findRow(ActionEvent event) {
        try {
            String str = " ";
            switch (tableName) {
                case "Patient":
                    p = pDAOImpl.findById(p.getPatientID());
                    break;
                case "InPatient":
                    ip = pDAOImpl.findInPatientBeanById(ip.getId());
                    break;
                case "Medication":
                    m = pDAOImpl.findMedBeanById(m.getID());
                    break;
                case "Surgical":
                    s = pDAOImpl.findSurgicalBeanById(s.getId());
                    break;
                default:
                    log.debug("undefined tableName");
                    //if not find tableName, need to check codes, if not find the rows to delete, need to check input info
                    Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("Warnings!! undefined tableName: " + tableName);
                    alert1.setHeaderText("check codes");
                    alert1.setContentText("check codes in controller for deleteRow()");
                    alert1.showAndWait();
                    break;
            }
        } catch (SQLException e) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Warnings!! SQL error when using findRow() in " + tableName + " table");
            alert.setHeaderText("Check your codes in findRow()");
            alert.setContentText("Check your codes please");
            alert.showAndWait();
        }

        log.debug("End of findRow");
    }

    @FXML
    void insertRow(ActionEvent event) {
        try {
            String str = " ";
            switch (tableName) {
                case "Patient":
                    if (!pDAOImpl.findById(p.getPatientID()).equals(p)) {
                        pDAOImpl.insertPatient(p);
                        break;
                    } else {
                        str = p.toString();
                    }
                case "InPatient":
                    if (!pDAOImpl.findInPatientBeanById(ip.getId()).equals(ip)) {
                        pDAOImpl.insertInp(ip);
                        break;
                    } else {
                        str = ip.toString();
                    }
                case "Medication":
                    if (!pDAOImpl.findMedBeanById(m.getID()).equals(m)) {
                        pDAOImpl.insertMed(m);
                        break;
                    } else {
                        str = m.toString();
                    }
                case "Surgical":
                    if (!pDAOImpl.findSurgicalBeanById(s.getId()).equals(s)) {
                        pDAOImpl.insertSur(s);
                        break;
                    } else {
                        str = s.toString();
                    }
                default:
                    log.debug("Your input has already existed in insertRow()!");
                    //if not find tableName, need to check codes, if not find the rows to delete, need to check input info
                    Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("Warnings!! input already exist in: " + tableName);
                    alert1.setHeaderText("change ID or other para, it is already in the database:");
                    alert1.setContentText(str);
                    alert1.showAndWait();
                    break;
            }
        } catch (SQLException e) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Warnings!! SQL error when using insertRow() in " + tableName + " table");
            alert.setHeaderText("Check your codes in insertRow()");
            alert.setContentText("Check your codes please");
            alert.showAndWait();
        }

        log.debug("End of insertRow");
    }

    @FXML
    void report(ActionEvent event) {
        int id, i, j;
        String str = " ", strTemp = " ";
        // find patient id first.
        switch (tableName) {
            case "Patient":
                id = p.getPatientID();
                break;
            case "InPatient":
                id = ip.getPatientId();
                break;
            case "Medication":
                id = m.getPatientID();
                break;
            case "Surgical":
                id = s.getPatientId();
                break;
            default:
                id = -1;
                break;
        }
        double d = 0, dInp = 0, dMed = 0, dSur = 0;
        try {
            // fees in InPatient table
            ipArrayList = pDAOImpl.findAllInPatient();
            for (i = 0; i < ipArrayList.size(); i++) {
                if (ipArrayList.get(i + 1).getPatientId() == id) {
                    //calculate how long he has stayed so far.
                    j = ipArrayList.get(i + 1).getDateOfStay().compareTo(Timestamp.valueOf(LocalDateTime.now()));
                    dInp += ipArrayList.get(i + 1).getServices() + ipArrayList.get(i + 1).getSupplies() + j * ipArrayList.get(i + 1).getDailyRate();
                    strTemp += "Fees in InPatient Table(id=" + ipArrayList.get(i + 1).getId();
                    strTemp += ": stayed so far " + j + " days, dailyRate " + ipArrayList.get(i + 1).getDailyRate() + ";";
                    strTemp += " Services = " + ipArrayList.get(i + 1).getServices() + " Supplies " + ipArrayList.get(i + 1).getSupplies() + " \n";
                    log.debug(strTemp);
                    str += strTemp;
                    strTemp = " ";
                }
            }
            // fees in Medication Table
            mArrayList = pDAOImpl.findAllMed();
            for (i = 0; i < mArrayList.size(); i++) {
                if (mArrayList.get(i + 1).getPatientID() == id) {
                    dMed += mArrayList.get(i + 1).getUnitCost() * mArrayList.get(i + 1).getUnits();
                    strTemp += "Fees in medication table (id=" + mArrayList.get(i + 1).getID();
                    strTemp += ": " + mArrayList.get(i + 1).getUnitCost() + "*" + mArrayList.get(i + 1).getUnits() + "\n";
                    log.debug(strTemp);
                    str += strTemp;
                    strTemp = " ";
                }
            }
            //fees in surgical
            sArrayList = pDAOImpl.findAllSurgical();
            for (i = 0; i < sArrayList.size(); i++) {
                if (sArrayList.get(i + 1).getPatientId() == id) {
                    dSur += sArrayList.get(i + 1).getSurgeonFee() + sArrayList.get(i + 1).getSupplies() + sArrayList.get(i + 1).getRoomFee();
                    strTemp += "Fees in surgical table (id=" + sArrayList.get(i + 1).getId();
                    strTemp += ": roomfee=" + sArrayList.get(i + 1).getRoomFee() + ";supplies=" + sArrayList.get(i + 1).getSupplies();
                    strTemp += "; surgeonFee = " + sArrayList.get(i + 1).getSurgeonFee() + "\n";
                    log.debug(strTemp);
                    str += strTemp;
                    strTemp = " ";
                }
            }
            d = dInp + dMed + dSur;
            log.debug("total fee for patient is " + d);
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Total Fee for PatientID=" + id + " is ");
            alert.setHeaderText(" " + d + " includes: Surgical:" + dSur + " Medication:" + dMed + " InPatient:" + dInp);
            alert.setContentText("Details:\n" + str);
            alert.showAndWait();
        } catch (SQLException e) {
            log.debug("SQLException in report() ");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("SQLException when reporting Total Fee for PatientID=" + id);
            alert.setHeaderText("Warning! SQLException when reporting total fee for patientID");
            alert.setContentText("Check Codes in controller:  report()\n");
            alert.showAndWait();
        }

    }

    @FXML
    void selectInPatientTable(ActionEvent event) {
        tableName = "InPatient";
        iPresent = 0;
        Bindings.bindBidirectional(inputPatternTextField.textProperty(), InPatientBean.sampleInfoProperty);
        //       Bindings.bindBidirectional(infoTitleTextField.textProperty(), InPatientBean.sampleInfoProperty);
        //binding InPatient object: dateOfStay, roomNumber, dailyRate, supplies, services
        Bindings.bindBidirectional(idInputTextField.textProperty(), ip.getIdProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(patientIdInputTextField.textProperty(), ip.getPatientIdProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField1.textProperty(), ip.getDateOfStayPropertyStr());
        Bindings.bindBidirectional(infoInputTextField2.textProperty(), ip.getRoomNumberProperty());
        Bindings.bindBidirectional(infoInputTextField3.textProperty(), ip.getDailyRateProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField4.textProperty(), ip.getSuppliesProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField5.textProperty(), ip.getServicesProperty(), new NumberStringConverter());
        log.debug("selectInPatientTable End");
         tableViewRenew();

    }

    @FXML
    void selectMedicationTable(ActionEvent event) {
        tableName = "Medication";
        iPresent = 0;
        Bindings.bindBidirectional(inputPatternTextField.textProperty(), MedBean.sampleInfoProperty);
        //     Bindings.bindBidirectional(infoTitleTextField.textProperty(), MedBean.sampleInfoProperty);
        //binding Medication object: "dateOfMed, med, unitCost, units"
        Bindings.bindBidirectional(idInputTextField.textProperty(), m.getIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(patientIdInputTextField.textProperty(), m.getPatientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField1.textProperty(), m.gedDateOfMedPropertyStr());
        Bindings.bindBidirectional(infoInputTextField2.textProperty(), m.getMedProperty());
        Bindings.bindBidirectional(infoInputTextField3.textProperty(), m.getUnitCostProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField4.textProperty(), m.getUnitsProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField5.textProperty(), null);
        log.debug("selectMedicationTable End");
         tableViewRenew();
    }

    @FXML
    void selectPatientTable(ActionEvent event) {
        tableName = "Patient";
        iPresent = 0;
        Bindings.bindBidirectional(inputPatternTextField.textProperty(), Patient.sampleInfoProperty);
        //      Bindings.bindBidirectional(infoTitleTextField.textProperty(), Patient.sampleInfoProperty);        
        //binding patient
        Bindings.bindBidirectional(idInputTextField.textProperty(), null, new NumberStringConverter());
        Bindings.bindBidirectional(patientIdInputTextField.textProperty(), p.getPatientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField1.textProperty(), p.getLastNameProperty());
        Bindings.bindBidirectional(infoInputTextField2.textProperty(), p.getFirstNameProperty());
        Bindings.bindBidirectional(infoInputTextField3.textProperty(), p.getDiagnosisProperty());
        Bindings.bindBidirectional(infoInputTextField4.textProperty(), p.getAdmissionDatePropertyStr());
        Bindings.bindBidirectional(infoInputTextField5.textProperty(), p.getReleaseDatePropertyStr());
        log.debug("selectPatientTable End");
         tableViewRenew();
    }

    @FXML
    void selectSurgicalTable(ActionEvent event) {
        tableName = "Surgical";
        iPresent = 0;
        Bindings.bindBidirectional(inputPatternTextField.textProperty(), SurgicalBean.sampleInfoProperty);
        //    Bindings.bindBidirectional(infoTitleTextField.textProperty(), SurgicalBean.sampleInfoProperty);        
        //binding Surgical:"dateOfSurgery, surgery, roomFee, surgeonFee, supplies" 
        Bindings.bindBidirectional(idInputTextField.textProperty(), s.getIdProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(patientIdInputTextField.textProperty(), s.getPatientIdProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField1.textProperty(), s.getDateOfSurgeryPropertyStr());
        Bindings.bindBidirectional(infoInputTextField2.textProperty(), s.getSurgeryProperty());
        Bindings.bindBidirectional(infoInputTextField3.textProperty(), s.getRoomFeeProperty(),new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField4.textProperty(), s.getSurgeonFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField5.textProperty(), s.getSuppliesProperty(), new NumberStringConverter());
        log.debug("selectSurgicalTable End");
        tableViewRenew();
    }

    @FXML
    void showNext(ActionEvent event) {
        int id, i;
        try {
            switch (tableName) {
                case "Patient":
                    pArrayList = pDAOImpl.findAll();
                    id = p.getPatientID();
                    p = pDAOImpl.findById(id);
                    i = pArrayList.indexOf(p);
                    if ((i + 1) >= pArrayList.size()) {
                        p = pArrayList.get(0);
                    } else {
                        p = pArrayList.get(i + 1);
                    }
                    break;
                case "InPatient":
                    ipArrayList = pDAOImpl.findAllInPatient();
                    id = ip.getId();
                    ip = pDAOImpl.findInPatientBeanById(id);
                    i = ipArrayList.indexOf(ip);
                    if ((i + 1) >= ipArrayList.size()) {
                        ip = ipArrayList.get(0);
                    } else {
                        ip = ipArrayList.get(i + 1);
                    }
                    break;
                case "Surgical":
                    sArrayList = pDAOImpl.findAllSurgical();
                    id = s.getId();
                    s = pDAOImpl.findSurgicalBeanById(id);
                    i = sArrayList.indexOf(s);
                    if ((i + 1) >= sArrayList.size()) {
                        s = sArrayList.get(0);
                    } else {
                        s = sArrayList.get(i + 1);
                    }
                    break;
                case "Medication":
                    mArrayList = pDAOImpl.findAllMed();
                    id = m.getID();
                    m = pDAOImpl.findMedBeanById(id);
                    i = mArrayList.indexOf(m);
                    if ((i + 1) >= mArrayList.size()) {
                        m = mArrayList.get(0);
                    } else {
                        m = mArrayList.get(i + 1);
                    }
                    break;
                default:
                    Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("no such table " + tableName);
                    alert1.showAndWait();
            }
        } catch (SQLException e) {
            log.debug("SQLException in next() ");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("SQLException in next() in controller");
            alert.setHeaderText("Warning! SQLException when next() in controller");
            alert.setContentText("Check Codes in controller:  next()\n");
            alert.showAndWait();
        }
    }

    private int presentSize() {
        int n = 0;
        switch (tableName) {
            case "Patient":
                n = pArrayList.size();
                break;
            case "InPatient":
                n = ipArrayList.size();
                break;
            case "Medication":
                n = mArrayList.size();
                break;
            case "Surgical":
                n = sArrayList.size();
                break;
            default:
                n = 1;
        }
        return n;
    }

    @FXML
    void showPrev(ActionEvent event) {
        int id, i;
        try {
            switch (tableName) {
                case "Patient":
                    pArrayList = pDAOImpl.findAll();
                    id = p.getPatientID();
                    p = pDAOImpl.findById(id);
                    i = pArrayList.indexOf(p);
                    if ((i - 1) < 0) {
                        p = pArrayList.get(pArrayList.size() - 1);
                    } else {
                        p = pArrayList.get(i - 1);
                    }
                    break;
                case "InPatient":
                    ipArrayList = pDAOImpl.findAllInPatient();
                    id = ip.getId();
                    ip = pDAOImpl.findInPatientBeanById(id);
                    i = ipArrayList.indexOf(ip);
                    if ((i - 1) < 0) {
                        ip = ipArrayList.get(ipArrayList.size() - 1);
                    } else {
                        ip = ipArrayList.get(i - 1);
                    }
                    break;
                case "Surgical":
                    sArrayList = pDAOImpl.findAllSurgical();
                    id = s.getId();
                    s = pDAOImpl.findSurgicalBeanById(id);
                    i = sArrayList.indexOf(s);
                    if ((i - 1) < 0) {
                        s = sArrayList.get(sArrayList.size() - 1);
                    } else {
                        s = sArrayList.get(i - 1);
                    }
                    break;
                case "Medication":
                    mArrayList = pDAOImpl.findAllMed();
                    id = m.getID();
                    m = pDAOImpl.findMedBeanById(id);
                    i = mArrayList.indexOf(m);
                    if ((i - 1) < 0) {
                        m = mArrayList.get(mArrayList.size() - 1);
                    } else {
                        m = mArrayList.get(i - 1);
                    }
                    break;
                default:
                    Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("no such table in prev()" + tableName);
                    alert1.showAndWait();
                    break;
            }
        } catch (SQLException e) {
            log.debug("SQLException in next() ");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("SQLException in prev() in controller");
            alert.setHeaderText("Warning! SQLException when prev() in controller");
            alert.setContentText("Check Codes in controller:  prevt()\n");
            alert.showAndWait();
        }
    }

    @FXML
    void findAll(ActionEvent event) {
             tableViewRenew();
    }

    @FXML
    void updateRow(ActionEvent event) {
        try {
            switch (tableName) {
                case "Patient":
                    if (pDAOImpl.findById(p.getPatientID()) != null) {
                        pDAOImpl.updatePatient(p);
                        break;
                    }
                case "InPatient":
                    if (pDAOImpl.findInPatientBeanById(ip.getId()) != null) {
                        pDAOImpl.updateInp(ip);
                        break;
                    }
                case "Medication":
                    if (pDAOImpl.findMedBeanById(m.getID()) != null) {
                        pDAOImpl.updateMed(m);
                        break;
                    }
                case "Surgical":
                    if (pDAOImpl.findSurgicalBeanById(s.getId()) != null) {
                        pDAOImpl.updateSur(s);
                        break;
                    }
                default:
                    log.debug("not find ID or PID in updateRow() ");
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Not Find IP or PID in updateRow() in controller");
                    alert.setHeaderText("Warning! recordes with your input id or pid, not exist");
                    alert.setContentText("If you need create, use create button, please\n");
                    alert.showAndWait();
            }
        } catch (SQLException e) {
            log.debug("SQLException in next() ");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("SQLException in updateRow() in controller");
            alert.setHeaderText("Warning! SQLException when updateRow() in controller");
            alert.setContentText("Check Codes in controller:  updateRow()\n");
            alert.showAndWait();
        }
    }

    @FXML
    private AnchorPane hospitalPane;
    
    @FXML // fx:id="infoInputTextField"
    private TextField infoInputTextField; // Value injected by FXMLLoader

    @FXML // fx:id="idInputTextField"
    private TextField idInputTextField; // Value injected by FXMLLoader

    @FXML // fx:id="patientIdInputTextField"
    private TextField patientIdInputTextField; // Value injected by FXMLLoader

    @FXML // fx:id="infoTitleTextField"
    private TextField infoTitleTextField; // Value injected by FXMLLoader

    @FXML
    private TextField inputPatternTextField;

    @FXML
    private TextField infoInputTextField1;

    @FXML
    private TextField infoInputTextField2;

    @FXML
    private TextField infoInputTextField3;

    @FXML
    private TextField infoInputTextField4;

    @FXML
    private TextField infoInputTextField5;

    @FXML
    private TableView<Patient> findAllTableView;

    @FXML
    private TableColumn<Patient, Number> tableColID;

    @FXML
    private TableColumn<Patient, Number> tableColPID;

    @FXML
    private TableColumn<Patient, String> tableColContent;

    private void tableViewRenew() {
        try {
            switch (tableName) {
                case "Patient":
                    pArrayList = pDAOImpl.findAll();
                    tableColID.setCellValueFactory(cellData -> null);
                    tableColPID.setCellValueFactory(cellData -> cellData.getValue().getPatientIDProperty());
                    tableColContent.setCellValueFactory(cellData -> cellData.getValue().getInfoProperty()); 
                    findAllTableView.setItems((ObservableList<Patient>) pArrayList);
                    break;
                case "InPatient":
                    ipArrayList = pDAOImpl.findAllInPatient();
////////////////////////////////how to change from TableView<Patient> to TableView<InPatient>/////////////////////////////////////////////////
     //               tableColPID.setCellValueFactory(cellData -> cellData.getValue().getPatientIDProperty());????????????????????????????????????
     //   tableColContent.setCellValueFactory(cellData -> cellData.getValue().getInfoProperty());         ??????????????   
                    break;
                case "Surgical":
                    sArrayList = pDAOImpl.findAllSurgical();
                    break;
                case "Medication":
                    mArrayList = pDAOImpl.findAllMed();
                    break;
                default:
                    log.debug("tableName not exist in tableViewRenew()");
                    Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("tableName not exist in tableViewRenew()");
                    alert1.setHeaderText("Warning! tableName not exist in tableViewRenew() in controller");
                    alert1.setContentText("tableName not exist in tableViewRenew()\n");
                    alert1.showAndWait();
            }
        } catch (SQLException e) {
            log.debug("SQLException in tableViewRenew() ");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("SQLException in tableViewRenew() in controller");
            alert.setHeaderText("Warning! SQLException when tableViewRenew() in controller");
            alert.setContentText("Check Codes in controller:  tableViewRenew() \n");
            alert.showAndWait();
        }

        	
		// Add observable list data to the table
		
	
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        iPresent = 0;
        tableName = "Patient";
        int i;

        //binding titles after selection of tables 
        Bindings.bindBidirectional(inputPatternTextField.textProperty(), Patient.sampleInfoProperty);
        //Bindings.bindBidirectional(infoTitleTextField.textProperty(), Patient.sampleInfoProperty);
        //binding report after selection of report
        //binding input paras: "lastName,firstName, diagnosis, admissionDate, releaseDate"        
        //Bindings.bindBidirectional(idInputTextField.textProperty(), null, new NumberStringConverter());
        Bindings.bindBidirectional(patientIdInputTextField.textProperty(), p.getPatientIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(infoInputTextField1.textProperty(), p.getLastNameProperty());
        Bindings.bindBidirectional(infoInputTextField2.textProperty(), p.getFirstNameProperty());
        Bindings.bindBidirectional(infoInputTextField3.textProperty(), p.getDiagnosisProperty());
        Bindings.bindBidirectional(infoInputTextField4.textProperty(), p.getAdmissionDatePropertyStr());
        Bindings.bindBidirectional(infoInputTextField5.textProperty(), p.getReleaseDatePropertyStr());
        //
        try {
            pArrayList = pDAOImpl.findAll();
        } catch (SQLException e) {
            log.debug("SQLException in initial()->pDAOImpl.findAll() ");
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("SQLException in initial()->pDAOImpl.findAll() in controller");
            alert.setHeaderText("Warning! SQLException when initial()->pDAOImpl.findAll() in controller");
            alert.setContentText("Check Codes in controller:  initial()->pDAOImpl.findAll()\n");
            alert.showAndWait();
        }
        //initial tableView
       tableColID.setCellValueFactory(cellData -> null);
        tableColPID.setCellValueFactory(cellData -> cellData.getValue().getPatientIDProperty());
        tableColContent.setCellValueFactory(cellData -> cellData.getValue().getInfoProperty());

        findAllTableView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showDataDetails(newValue));
        tableViewRenew();

///
    }
	public void displayTheTable() throws SQLException {
		// Add observable list data to the table
		findAllTableView.setItems((ObservableList<Patient>) this.pDAOImpl.findAll());
	}
    private void showDataDetails(Object p) {
        System.out.println(p);
    }
}
