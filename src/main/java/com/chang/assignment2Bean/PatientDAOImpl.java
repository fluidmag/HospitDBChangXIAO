/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PatientDAOImpl implements PatientDAO {
//	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private final Logger log = Logger.getLogger(this.getClass().getName());

    // This information should be coming from a Properties file
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "root";
    private final String password = "dawson";

    /**
     * deleteTemplate used as a template for delete rows in tables and can be
     * applied to delete rows in different tables
     *
     * @param id
     * @param tableName
     * @param itemName
     * @return
     * @throws SQLException
     */
          
      private SurgicalBean createSurgicalBean(ResultSet resultSet) throws SQLException{
 
        
        SurgicalBean p = new SurgicalBean();
        p.setDateOfSurgery(resultSet.getTimestamp("DATEOFSURGERY"));
        p.setId(resultSet.getInt("ID"));
        p.setPatientId(resultSet.getInt("PATIENTID"));
        p.setRoomFee(resultSet.getDouble("ROOMFEE"));
        p.setSupplies(resultSet.getDouble("SUPPLIES"));
        p.setSurgeonFee(resultSet.getDouble("SURGEONFEE"));
        p.setSurgery(resultSet.getString("SURGERY"));
        return p;
      }   
      private MedBean createMedBean(ResultSet resultSet) throws SQLException {
   
		MedBean p = new MedBean();
                p.setDateOfMed(resultSet.getTimestamp("DATEOFMED"));
		p.setID(resultSet.getInt("ID"));
		p.setMed(resultSet.getString("MED"));
		p.setPatientID(resultSet.getInt("PATIENTID"));
		p.setUnitCost(resultSet.getDouble("UNITCOST"));
		p.setUnits(resultSet.getDouble("UNITS"));
         
		return p;
	} 
	private Patient createPatient(ResultSet resultSet) throws SQLException {
		Patient p = new Patient();
		p.setPatientID(resultSet.getInt("PATIENTID"));
		p.setLastName(resultSet.getString("LASTNAME"));
		p.setFirstName(resultSet.getString("FIRSTNAME"));
		p.setDiagnosis(resultSet.getString("DIAGNOSIS"));
		p.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
		p.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                
		return p;
	}    
      
        private int createID(ResultSet resultSet) throws SQLException {
            return resultSet.getInt("ID");        
        }

	private InPatientBean createInPatientBean(ResultSet resultSet) throws SQLException {
   
		InPatientBean p = new InPatientBean();
                p.setId(resultSet.getInt("ID"));
		p.setPatientId(resultSet.getInt("PATIENTID"));
		p.setDateOfStay(resultSet.getTimestamp("DATEOFSTAY"));
		p.setRoomNumber(resultSet.getString("ROOMNUMBER"));
		p.setDailyRate(resultSet.getDouble("DAILYRATE"));
		p.setSupplies(resultSet.getDouble("SUPPLIES"));
		p.setServices(resultSet.getDouble("SERVICES"));
                
		return p;
	}           
    private int deleteTemplate(int id, String tableName, String itemName) throws SQLException {
        int result = 0;
        String deleteQuery = "DELETE FROM " + tableName + " WHERE " + itemName + " = ?";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteMed(int id) throws SQLException {
        return (deleteTemplate(id, "MEDICATION", "ID"));
    }

    @Override
    public int deleteSur(int id) throws SQLException {
        return (deleteTemplate(id, "SURGICAL", "ID"));
    }

    @Override
    public int deleteInp(int id) throws SQLException {
        return (deleteTemplate(id, "INPATIENT", "ID"));
    }
//         String createQuery = "INSERT INTO " + "INPATIENT"
     //           + " ( PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES) VALUES (?,?,?,?,?,?)";//
    public int insertMed(MedBean b) throws SQLException {
        int result = 0;
        String createQuery = "INSERT INTO " + "MEDICATION"
                + " (PATIENTID, DATEOFMED, MED, UNITCOST, UNITS) VALUES (?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
            PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(b.getPatientID()));
            ps.setString(2, b.getDateOfMed().toString());
            ps.setString(3, b.getMed());
            ps.setString(4, Double.toString(b.getUnitCost()));
            ps.setString(5, Double.toString(b.getUnits()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        
        return result;
    }

    @Override
    public int insertSur(SurgicalBean b) throws SQLException {
        int result = 0;
        String createQuery = "INSERT INTO " + "SURGICAL"
                + " ( PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES) VALUES (?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(b.getPatientId()));
            ps.setString(2, b.getDateOfSurgery().toString());
            ps.setString(3, b.getSurgery());
            ps.setString(4, Double.toString(b.getRoomFee()));
            ps.setString(5, Double.toString(b.getSurgeonFee()));
            ps.setString(6, Double.toString(b.getSupplies()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;
    }

    @Override
    public int insertInp(InPatientBean b) throws SQLException {
        int result = 0;
                    System.out.println(b.getDateOfStay());
        String createQuery = "INSERT INTO " + "INPATIENT"
                + " ( PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES) VALUES (?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
           
            ps.setString(1, Integer.toString(b.getPatientId()));

            ps.setString(2, b.getDateOfStay().toString());
            
            ps.setString(3, b.getRoomNumber());
            ps.setString(4, Double.toString(b.getDailyRate()));
            ps.setString(5, Double.toString(b.getSupplies()));
            ps.setString(6, Double.toString(b.getServices()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
       // System.out.println("id="+result);
        return result;
    }

    @Override
    public int updateMed(MedBean b) throws SQLException {
        int result = 0;

        String updateQuery = "UPDATE MEDICATION SET PATIENTID=?, DATEOFMED=?, MED=?, UNITCOST=?, UNITS=? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, Integer.toString(b.getPatientID()));
            ps.setString(2, b.getDateOfMed().toString());
            ps.setString(3, b.getMed());
            ps.setString(4, Double.toString(b.getUnitCost()));
            ps.setString(5, Double.toString(b.getUnits()));
            ps.setString(6, Integer.toString(b.getID()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }
@Override
public int updateSur(SurgicalBean b)throws SQLException {
        int result = 0;
        String updateQuery = "UPDATE SURGICAL SET PATIENTID=?, DATEOFSURGERY=?, SURGERY=?, ROOMFEE=?, SURGEONFEE=?, SUPPLIES = ? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, Integer.toString(b.getPatientId()));
            ps.setString(2, b.getDateOfSurgery().toString());
            ps.setString(3, b.getSurgery());
            ps.setString(4, Double.toString(b.getRoomFee()));
            ps.setString(5, Double.toString(b.getSurgeonFee()));
            ps.setString(6, Double.toString(b.getSupplies()));
            ps.setString(7, Integer.toString(b.getId()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }
        
        
    @Override
    public Timestamp dateSur(int id) throws SQLException {
        Timestamp result;

        String strQuery = "SELECT DATEOFSURGERY FROM SURGICAL WHERE ID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareCall(strQuery);
                ){
            ps.setString(1,Integer.toString(id));            
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                result = rs.getTimestamp("DATEOFSURGERY");
            else 
                result = Timestamp.valueOf(LocalDateTime.now());
            
        }
        return result;
    }

    @Override
    public int updateInp(InPatientBean b) throws SQLException {
        int result = 0;

        String updateQuery = "UPDATE INPATIENT SET PATIENTID=?, DATEOFSTAY=?, ROOMNUMBER=?, DAILYRATE=?, SUPPLIES=?, SERVICES=? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, Integer.toString(b.getPatientId()));
            ps.setString(2, b.getDateOfStay().toString());
            ps.setString(3, b.getRoomNumber());
            ps.setString(4, Double.toString(b.getDailyRate()));
            ps.setString(5, Double.toString(b.getSupplies()));
            ps.setString(6, Double.toString(b.getServices()));
            ps.setString(7, Integer.toString(b.getId()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }
     @Override
    public MedBean findMedBeanById(int key) throws SQLException {
            MedBean p = new MedBean();
   
        String strQuery = "SELECT ID, PATIENTID, DATEOFMED, MED, UNITCOST, "
                + "UNITS FROM MEDICATION WHERE ID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(key));
            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					p = createMedBean(resultSet);
				}
			}
        }
        if(key==p.getID()){
        return p;}
        else{return null;}
    }

    @Override
    public SurgicalBean findSurgicalBeanById(int key) throws SQLException {
              SurgicalBean p = new SurgicalBean();
 
        String strQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, "
                + "SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(key));
            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					p = createSurgicalBean(resultSet);
				}
			}
        }
        if(key==p.getId()){
        return p;}
        else{return null;}
    }

   @Override
    public InPatientBean findInPatientBeanById(int key) throws SQLException{
        InPatientBean p = new InPatientBean();
   
        String strQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, "
                + "SUPPLIES, SERVICES FROM INPATIENT WHERE ID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(key));
            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					p = createInPatientBean(resultSet);
				}
			}
        }
        if(key==p.getId()){
        return p;}
        else{return null;}
    }
    
    
    @Override
    public Patient findById(int key) throws SQLException{
        Patient p = new Patient();
        String strQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, "
                + "ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(key));
            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					p = createPatient(resultSet);
				}
			}
        }
         if(key==p.getPatientID()){return p;}
         else{return null;}
        
    }
    @Override
    public int fixInPatientBeanID(InPatientBean b) throws SQLException{
        int id = b.getId();
        
        String strQuery = "SELECT ID FROM INPATIENT WHERE PATIENTID = ? AND DATEOFSTAY = ? AND ROOMNUMBER = ? AND DAILYRATE = ? AND "
                + "SUPPLIES = ? AND SERVICES = ?";
              try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(b.getPatientId()));
            ps.setString(2, b.getDateOfStay().toString());
            ps.setString(3, b.getRoomNumber());
            ps.setString(4, Double.toString(b.getDailyRate()));
            ps.setString(5, Double.toString(b.getSupplies()));
            ps.setString(6, Double.toString(b.getServices()));

            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					id = createID(resultSet);
                                        if(id==b.getId()){return id;}
                                        else{b.setId(id); return id;}
				}
			}
        }
         if(id==b.getId()){;}
         else{b.setId(id); }
      return id;
    }
       //     String strQuery = "SELECT ID, PATIENTID, DATEOFMED, MED, UNITCOST, "
         //       + "UNITS FROM MEDICATION WHERE ID = ?";
    @Override
    public int fixMedBeanID(MedBean b) throws SQLException{
        int id = b.getID();
        String strQuery = "SELECT ID FROM MEDICATION WHERE PATIENTID = ? AND DATEOFMED=? AND MED=? AND UNITCOST=? AND UNITS=? ";
              try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(b.getPatientID()));
            ps.setString(2, b.getDateOfMed().toString());
            ps.setString(3, b.getMed());
            ps.setString(4, Double.toString(b.getUnitCost()));
            ps.setString(5, Double.toString(b.getUnits()));

            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					id = createID(resultSet);
                                        if(id==b.getID()){return id;}
                                        else{b.setID(id); return id;}
				}
			}
        }
         if(id==b.getID()){id=-9999;}
         else{b.setID(id); }
      return id;
    }
    //        String strQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, "
    //            + "SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = ?";
    @Override
    public int fixSurgicalBeanID(SurgicalBean b) throws SQLException{
        int id = b.getId();
        String strQuery = "SELECT ID FROM SURGICAL WHERE PATIENTID = ? AND DATEOFSURGERY=? AND SURGERY=? AND ROOMFEE=? AND SURGEONFEE=? AND SUPPLIES = ? ";
              try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(b.getPatientId()));
            ps.setString(2, b.getDateOfSurgery().toString());
            ps.setString(3, b.getSurgery());
            ps.setString(4, Double.toString(b.getRoomFee()));
            ps.setString(5, Double.toString(b.getSurgeonFee()));
            ps.setString(6, Double.toString(b.getSupplies()));

            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					id = createID(resultSet);
                                        if(id==b.getId()){return id;}
                                        else{b.setId(id); return id;}
				}
			}
        }
         if(id==b.getId()){id = -99999;}
         else{b.setId(id); }
      return id;
    }
    
    
    @Override
    public ObservableList<Patient> findAll() throws SQLException{
        ObservableList<Patient> pList = FXCollections.observableArrayList();
        //ArrayList<Patient> pList = new ArrayList<>();
  		String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT";
		// Using try with resources
		// This ensures that the objects in the parenthesis () will be closed
		// when block ends. In this case the Connection, PreparedStatement and
		// the ResultSet will all be closed.
		try (Connection connection = DriverManager.getConnection(url, user, password);
				// must use PreparedStatements to guard against SQL
				// Injection
				PreparedStatement pStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {
				pList.add(createPatient(resultSet));
			}
		}
		log.info("# of records found : " + pList.size());
		return pList;
    }
    
    @Override
    public ArrayList<InPatientBean> findAllInPatient() throws SQLException{
        ArrayList<InPatientBean> pList = new ArrayList<>();
  		String selectQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, "
                + "SUPPLIES, SERVICES FROM INPATIENT";
		// Using try with resources
		// This ensures that the objects in the parenthesis () will be closed
		// when block ends. In this case the Connection, PreparedStatement and
		// the ResultSet will all be closed.
		try (Connection connection = DriverManager.getConnection(url, user, password);
				// must use PreparedStatements to guard against SQL
				// Injection
				PreparedStatement pStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {
				pList.add(createInPatientBean(resultSet));
			}
		}
		log.info("# of records found : " + pList.size());
		return pList;
    }
    
    @Override
    public ArrayList<MedBean> findAllMed() throws SQLException{
        ArrayList<MedBean> pList = new ArrayList<>();
  		String selectQuery = "SELECT ID, PATIENTID, DATEOFMED, MED, UNITCOST, "
                + "UNITS FROM MEDICATION";
		// Using try with resources
		// This ensures that the objects in the parenthesis () will be closed
		// when block ends. In this case the Connection, PreparedStatement and
		// the ResultSet will all be closed.
		try (Connection connection = DriverManager.getConnection(url, user, password);
				// must use PreparedStatements to guard against SQL
				// Injection
				PreparedStatement pStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {
				pList.add(createMedBean(resultSet));
			}
		}
		log.info("# of records found : " + pList.size());
		return pList;
    }
    
    @Override
    public ArrayList<SurgicalBean> findAllSurgical() throws SQLException{
        ArrayList<SurgicalBean> pList = new ArrayList<>();
  		String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, "
                + "SURGEONFEE, SUPPLIES FROM SURGICAL";
		// Using try with resources
		// This ensures that the objects in the parenthesis () will be closed
		// when block ends. In this case the Connection, PreparedStatement and
		// the ResultSet will all be closed.
		try (Connection connection = DriverManager.getConnection(url, user, password);
				// must use PreparedStatements to guard against SQL
				// Injection
				PreparedStatement pStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {
				pList.add(createSurgicalBean(resultSet));
			}
		}
		log.info("# of records found : " + pList.size());
		return pList;
    }
    
    
    @Override
    public int updatePatient(Patient p) throws SQLException {
        int result = 0;

        String updateQuery = "UPDATE PATIENT SET LASTNAME=?, FIRSTNAME=?, DIAGNOSIS=?, ADMISSIONDATE=?, RELEASEDATE=? WHERE PATIENTID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {

            ps.setString(1, p.getLastName());
            ps.setString(2, p.getFirstName());
            ps.setString(3, p.getDiagnosis());
            ps.setString(4, p.getAdmissionDate().toString());
            ps.setString(5, p.getReleaseDate().toString());
            ps.setString(6, Integer.toString(p.getPatientID()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }

    @Override
    public int insertPatient(Patient p)throws SQLException{
        int result = 0;
        String createQuery = "INSERT INTO " + "PATIENT"
                + " (PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) VALUES (?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(p.getPatientID()));
            ps.setString(2, p.getLastName());
            ps.setString(3, p.getFirstName());
            ps.setString(4, p.getDiagnosis());
            ps.setString(5, p.getAdmissionDate().toString());
            ps.setString(6, p.getReleaseDate().toString());

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;  
    }

    @Override
    public int deletePatient(Patient p) throws SQLException{
      return deleteTemplate(p.getPatientID(), "PATIENT", "PATIENTID"); 
    }

   
}
