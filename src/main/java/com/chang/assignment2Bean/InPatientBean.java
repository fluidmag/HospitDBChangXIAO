/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.Timestamp;
import java.util.Objects;
import javafx.beans.property.*;

/**
 *this class is for the table inpatient
 * @author cha_xi
 */
public class InPatientBean {
    private IntegerProperty id;
    private IntegerProperty patientId;
    private ObjectProperty<Timestamp> dateOfStay;
    private StringProperty roomNumber;
    private DoubleProperty dailyRate;
    private DoubleProperty supplies;
    private DoubleProperty services;
    private StringProperty dateOfStayStr;
    public static StringProperty sampleInfoProperty = new SimpleStringProperty("dateOfStay, roomNumber, dailyRate, supplies, services");

    
    public InPatientBean(int id, int patientId, Timestamp dateOfStay, String roomNumber, double dailyRate, double supplies, double services) {
        this.id = new SimpleIntegerProperty(id);
        this.patientId= new SimpleIntegerProperty(patientId);
        this.dateOfStay = new SimpleObjectProperty<Timestamp>(dateOfStay);
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.dailyRate = new SimpleDoubleProperty(dailyRate);
        this.supplies = new SimpleDoubleProperty(supplies);
        this.services = new SimpleDoubleProperty(services);
        this.dateOfStayStr = new SimpleStringProperty(dateOfStay.toString());
    }

   public InPatientBean() {
        this.id = new SimpleIntegerProperty();
        this.patientId= new SimpleIntegerProperty();
        this.dateOfStay = new SimpleObjectProperty<Timestamp>();
        this.roomNumber = new SimpleStringProperty();
        this.dailyRate = new SimpleDoubleProperty();
        this.supplies = new SimpleDoubleProperty();
        this.services = new SimpleDoubleProperty();
        this.dateOfStayStr = new SimpleStringProperty();
    }
   
   
    public int getId() {
        return id.get();
    }
    
    public IntegerProperty getIdProperty() {
        return id;
    }
    public void setId(int id) {
        this.id.set(id);
    }

    public int getPatientId() {
        return patientId.get();
    }
    public IntegerProperty getPatientIdProperty() {
        return patientId;
    }
    
    public void setPatientId(int patientId) {
        this.patientId.set(patientId);
    }

    public Timestamp getDateOfStay() {
        return dateOfStay.get();
    }
    
    public StringProperty getDateOfStayPropertyStr(){
        dateOfStayStr.set(dateOfStay.toString());
        return dateOfStayStr;
    }
    public ObjectProperty<Timestamp> getDateOfStayProperty() {
        return dateOfStay;
    }    
    
    public void setDateOfStay(Timestamp dateOfStay) {
        this.dateOfStay.set(dateOfStay);
    }

    public String getRoomNumber() {
        return roomNumber.get();
    }

    public StringProperty getRoomNumberProperty() {
        return roomNumber;
    }    
    
    public void setRoomNumber(String roomNumber) {
        this.roomNumber.set(roomNumber);
    }

    public double getDailyRate() {
        return dailyRate.get();
    }

    public DoubleProperty getDailyRateProperty() {
        return dailyRate;
    }    
    
    public void setDailyRate(double dailyRate) {
        this.dailyRate.set(dailyRate);
    }

    public double getSupplies() {
        return supplies.get();
    }

    public DoubleProperty getSuppliesProperty() {
        return supplies;
    }    
    
    public void setSupplies(double supplies) {
        this.supplies.set(supplies);
    }

    public double getServices() {
        return services.get();
    }

    public DoubleProperty getServicesProperty() {
        return services;
    }    
    
    
    public void setServices(double services) {
        this.services.set(services);
    }

    @Override
    public String toString() {
        return "InPatientBean{" + "id=" + id.get() 
                + ", patientId=" + patientId.get() + ", dateOfStay="
                + dateOfStay.get().toString() + ", roomNumber=" + roomNumber.get() 
                + ", dailyRate=" + dailyRate.get() + ", supplies=" + supplies.get()
                + ", services=" + services.get() + '}';
    }

    public StringProperty getInfoProperty(){
        StringProperty sp = new SimpleStringProperty();
        sp.set("dateOfStay="
                + dateOfStay.get().toString() + ", roomNumber=" + roomNumber.get() 
                + ", dailyRate=" + dailyRate.get() + ", supplies=" + supplies.get()
                + ", services=" + services.get());
        return sp;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.id.get();
        hash = 83 * hash + this.patientId.get();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InPatientBean other = (InPatientBean) obj;
        if (this.id.get() != other.id.get()) {
            return false; 
        }
        if (this.patientId.get() != other.patientId.get()) {
            return false;
        }
        if (!this.dateOfStay.get().equals(other.dateOfStay.get())) {
            return false;
        }
        if (!this.roomNumber.get().equalsIgnoreCase(other.roomNumber.get())) {
            return false;
        }
        if (this.getDailyRate()!= other.dailyRate.get()) {
            return false;
        }
        if (this.supplies.get() != other.supplies.get()) {
            return false;
        }
        if (this.services.get() != other.services.get()) {
            return false;
        }
        return true;
    }

  

   

   
    
    
}
