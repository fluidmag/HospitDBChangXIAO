/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import javafx.collections.ObservableList;

/**
 *
 * @author cha_xi
 */
public interface PatientDAO {
    public int deleteMed(int id) throws SQLException;
        
    public int deleteSur(int id) throws SQLException;
    public int deleteInp(int id) throws SQLException;
    
    public int insertMed(MedBean b) throws SQLException;
    public int insertSur(SurgicalBean b) throws SQLException;
    public int insertInp(InPatientBean b) throws SQLException;
    
    public int updateMed(MedBean b)throws SQLException;
    public Timestamp dateSur(int id)throws SQLException;
    public int updateInp(InPatientBean b)throws SQLException;
    public int updateSur(SurgicalBean b)throws SQLException;
    
    public InPatientBean findInPatientBeanById(int key) throws SQLException;
    public MedBean findMedBeanById(int key) throws SQLException;
    public SurgicalBean findSurgicalBeanById(int key) throws SQLException;

    public int fixInPatientBeanID(InPatientBean p) throws SQLException;
    public int fixMedBeanID(MedBean p) throws SQLException;
    public int fixSurgicalBeanID(SurgicalBean p) throws SQLException;

    public Patient findById(int key) throws SQLException;
    public ObservableList<Patient> findAll() throws SQLException;
    public ArrayList<InPatientBean> findAllInPatient() throws SQLException;
    public ArrayList<MedBean> findAllMed() throws SQLException;
    public ArrayList<SurgicalBean> findAllSurgical() throws SQLException;
    
    public int updatePatient(Patient p)throws SQLException;
    public int insertPatient(Patient p)throws SQLException;
    public int deletePatient(Patient p)throws SQLException;
}
