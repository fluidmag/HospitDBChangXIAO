/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.Timestamp;
import java.util.Objects;
import javafx.beans.property.*;

/**
 *
 * @author cha_xi
 */
public class Patient {
    private IntegerProperty patientID;
    private StringProperty lastName;
    private StringProperty firstName;
    private StringProperty diagnosis;
    private ObjectProperty<Timestamp> admissionDate;
    private StringProperty admissionDateStr;
    private ObjectProperty<Timestamp> releaseDate;
    private StringProperty releaseDateStr;
    public static StringProperty sampleInfoProperty = new SimpleStringProperty("lastName,firstName, diagnosis, admissionDate, releaseDate");
    
   
    public Patient(int patientID, String lastName, String firstName, String diagnosis, Timestamp admissionDate, Timestamp releaseDate) {
        this.patientID = new SimpleIntegerProperty(patientID);
        this.lastName = new SimpleStringProperty(lastName);
        this.firstName = new SimpleStringProperty(firstName);
        this.diagnosis = new SimpleStringProperty(diagnosis);
        this.admissionDate = new SimpleObjectProperty<>(admissionDate);
        this.releaseDate = new SimpleObjectProperty<>(releaseDate);
        this.admissionDateStr = new SimpleStringProperty(admissionDate.toString());
        this.releaseDateStr = new SimpleStringProperty(releaseDate.toString());
    }

  
    public Patient() {
        this.patientID = new SimpleIntegerProperty();
        this.lastName = new SimpleStringProperty();
        this.firstName = new SimpleStringProperty();
        this.diagnosis = new SimpleStringProperty();
        this.admissionDate = new SimpleObjectProperty<>();
        this.releaseDate = new SimpleObjectProperty<>();
        this.releaseDateStr = new SimpleStringProperty();
        this.admissionDateStr = new SimpleStringProperty();
    }
    
    

    public IntegerProperty getPatientIDProperty() {
        return patientID;
    }    
    
    public int getPatientID() {
        return patientID.get();
    }

    public void setPatientID(int patientID) {
        this.patientID.set(patientID);
    }
    
    public StringProperty getLastNameProperty() {
        return lastName;
    }
    
    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getFirstName() {
        return firstName.get();
    }

     public StringProperty getFirstNameProperty() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getDiagnosis() {
        return diagnosis.get();
    }

    
  public StringProperty getDiagnosisProperty() {
        return diagnosis;
    }
        
    public void setDiagnosis(String diagnosis) {
        this.diagnosis.set(diagnosis);
    }

    public Timestamp getAdmissionDate() {
        return admissionDate.get();
    }
    public StringProperty getAdmissionDatePropertyStr(){
        admissionDateStr.set(admissionDate.toString());
        return admissionDateStr;
    }
    public ObjectProperty<Timestamp> getAdmissionDateProperty() {
        return admissionDate;
    }    
    
    public void setAdmissionDate(Timestamp admissionDate) {
        this.admissionDate.set(admissionDate);
    }

    public Timestamp getReleaseDate() {
        return releaseDate.get();
    }

    public StringProperty getReleaseDatePropertyStr(){
        releaseDateStr.set(releaseDate.toString());
        return releaseDateStr;
    }
    public ObjectProperty<Timestamp> getReleaseDateProperty() {
        return releaseDate;
    }    
    
    public void setReleaseDate(Timestamp releaseDate) {
        this.releaseDate.set(releaseDate);
    }

    @Override
    public String toString() {
        return "Patient{" + "patientID=" + patientID.get() + ", lastName=" + lastName.get()
                + ", firstName=" + firstName.get() + ", diagnosis=" + diagnosis.get()
                + ", admissionDate=" + admissionDate.get().toString() + 
                ", releaseDate=" + releaseDate.get().toString() + '}';
    }

    public StringProperty getInfoProperty(){
        StringProperty sp = new SimpleStringProperty();
        sp.set( "lastName=" + lastName.get()
                + ", firstName=" + firstName.get() + ", diagnosis=" + diagnosis.get()
                + ", admissionDate=" + admissionDate.get().toString() + 
                ", releaseDate=" + releaseDate.get().toString());
        return sp;
    }

    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.patientID.get();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Patient other = (Patient) obj;
        if (this.patientID.get() != other.patientID.get()) {
            return false;
        }
        if (!Objects.equals(this.lastName.get(), other.lastName.get())) {
            return false;
        }
        if (!Objects.equals(this.firstName.get(), other.firstName.get())) {
            return false;
        }
        if (!Objects.equals(this.diagnosis.get(), other.diagnosis.get())) {
            return false;
        }
        if (!Objects.equals(this.admissionDate.get(), other.admissionDate.get())) {
            return false;
        }
        if (!Objects.equals(this.releaseDate.get(), other.releaseDate.get())) {
            return false;
        }
        return true;
    }
    
}
